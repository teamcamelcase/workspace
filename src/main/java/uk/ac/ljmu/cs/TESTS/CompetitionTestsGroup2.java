package uk.ac.ljmu.cs.TESTS;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService.IaaSHandlingException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.unimiskolc.iit.distsys.ExercisesBase;
import hu.unimiskolc.iit.distsys.PMPriceRecord;
import hu.unimiskolc.iit.distsys.forwarders.PMForwarder;

public class CompetitionTestsGroup2 {

	@Test //1
	public void testReduction() throws Exception 
	{
		//Our first test is for reducing the number of machines on our service
		IaaSService underlyingDataCentre = ExercisesBase.getComplexInfrastructure(30);
		OurCloudProvider myVeryFirstProvider= new OurCloudProvider();
		myVeryFirstProvider.reduceMachines(underlyingDataCentre);
		assertEquals("Machine count not reduced by 15", 15, underlyingDataCentre.machines.size());
	}
	
	@Test //2
	public void testIncrease() throws Exception
	{
		//This test is for adding machines
		IaaSService underlyingDataCentre = ExercisesBase.getComplexInfrastructure(30);
		OurCloudProvider myVeryFirstProvider = new OurCloudProvider();
		myVeryFirstProvider.increaseIaaSMachines(underlyingDataCentre);
		assertEquals("Machine count not increased by 15", 45, underlyingDataCentre.machines.size());
	}
	
	@Test //3
	//This test if we can reduce the energy consumption of our machines
	public void lessConsumingIaaS() throws IllegalArgumentException, SecurityException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException, IaaSHandlingException {
		final double energyReduction = 1000; // W
		IaaSService service = ExercisesBase.getComplexInfrastructure(30);
		double energyConsumptionBefore = getTotalConsumptionForIaaS(service);
		System.out.println(energyConsumptionBefore);
		OurCloudProvider OurCloudProviderr = new OurCloudProvider();
		OurCloudProviderr.reduceEnergy(service, energyReduction);
		double consumptionAfter = getTotalConsumptionForIaaS(service);
		assertTrue("The reduction did not occur, the power draw of all the machines in the IaaS needs to be lower by a "+ energyReduction + "W", consumptionAfter + energyReduction < energyConsumptionBefore);
		System.out.println(energyConsumptionBefore);
		System.out.println(consumptionAfter);
	}
	
	// A method for getting the total energy consumption of service 
	public double getTotalConsumptionForIaaS(IaaSService forWhatService) {
		double totalConsumption = 0;
		for ( int i = 0; i < forWhatService.machines.size(); i++ ) {
			PhysicalMachine pm = forWhatService.machines.get(i);
			totalConsumption = totalConsumption + ((PMForwarder) pm).getMaxConsumption();
		}
		return totalConsumption;
	}
	
	@Test //4
	//A test for getting a lower priced machine
	public void lowerPricedMachine() throws SecurityException, InstantiationException, IllegalAccessException, NoSuchFieldException {
		OurCloudProvider OurCloudProviderr = new OurCloudProvider();
		
		PMForwarder basicPMs = (PMForwarder) ExercisesBase.getNewPhysicalMachine();	//Create new physical machine cast as a PMForwarder
		PMForwarder lowerEnergyPMs = OurCloudProviderr.createLowerPricedMachine(basicPMs);
		
		PMPriceRecord basicRecord=new PMPriceRecord(basicPMs);
		PMPriceRecord newRecord=new PMPriceRecord(lowerEnergyPMs);
		double basicPrice = basicRecord.getCurrentMachinePrice();
		double newPrice = newRecord.getCurrentMachinePrice();
		assertTrue("Please improve the price of the machine I am asking from you", newPrice < basicPrice);
	}
	
	@Test //5
	//A Test for improving the consumption of a machine
	public void checkIfLowerEnergyMachineIsReturned() throws SecurityException, InstantiationException, IllegalAccessException, NoSuchFieldException {		
		OurCloudProvider OurCloudProviderr = new OurCloudProvider();
		
		// we create a basic pm we then cast it as a PMForwarder to get more functions
		PMForwarder basicPMs = (PMForwarder) ExercisesBase.getNewPhysicalMachine();
		PMForwarder lowerEnergyPMs = OurCloudProviderr.createLowerEnergyMachine(basicPMs);
		
		//Finds the max consumption of the Physical Machine and lower energy machine to compare consumption
		double basicConsumption = basicPMs.getMaxConsumption();		
		double newConsumption = lowerEnergyPMs.getMaxConsumption();
		
		//Assert checks new consumption less than old consumption of energy
		assertTrue("Please improve the consumption of the machine I am asking from you", newConsumption  < basicConsumption);
	}
	
	//Test for provider reduction using class MyReactiveProvider
	@Test(timeout=100) //6
	public void testReduction15Providers() throws Exception {
		IaaSService iaas = ExercisesBase.getComplexInfrastructure(30);
		OurCloudProvider prov = new OurCloudProvider();
		prov.reduceMachines(iaas);
		assertEquals("Reactive provider reduction not reducing by 15", 15, iaas.machines.size());
	}
	
	
}