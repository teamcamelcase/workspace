package uk.ac.ljmu.cs.TESTS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService.IaaSHandlingException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.StateChange;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.ExercisesBase;
import hu.unimiskolc.iit.distsys.PMPriceRecord;
import hu.unimiskolc.iit.distsys.forwarders.IaaSForwarder;
import hu.unimiskolc.iit.distsys.forwarders.IaaSForwarder.VMListener;
import hu.unimiskolc.iit.distsys.forwarders.PMForwarder;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class OurCloudProvider implements CloudProvider, VMManager.CapacityChangeEvent<PhysicalMachine>, VMListener, StateChange{

		
		//Declare global variables for later use, most just empty variables to be used by whole class
		public IaaSService thisIaas;
		int vmsRequested = 0;
		int vmsDestroyedByUser = 0;
		HashMap<VirtualMachine, PhysicalMachine> vmHosts = new HashMap<VirtualMachine, PhysicalMachine>();
		VMListener otherListener = null;

		
		//Generated perTickQuote based on certain requirements
		@Override
		public double getPerTickQuote(ResourceConstraints rc) {
			int NumberOfVMs = 0;
			double energyUsed = 0;
			double startingPrice = 1;
			double total = 0;
			
			//Utilisation gets the total the current processing power and divides it by overall power to see how much we are "utilising"
			double utilization = thisIaas.getRunningCapacities().getTotalProcessingPower()
					/ thisIaas.getCapacities().getTotalProcessingPower();

			
			//This loop counts up each machine and how much energy its using and how many Vms its running to work out a good quote based on usage
			for (int i = 0; i < thisIaas.machines.size(); i++) {
				PhysicalMachine pm = thisIaas.machines.get(i);
				energyUsed = energyUsed + pm.getPerTickProcessingPower();
				NumberOfVMs = NumberOfVMs + pm.numofCurrentVMs();
				total = total + rc.getRequiredCPUs() * startingPrice * utilization * NumberOfVMs;
			}

			//Once all calculated and added up the perTickQuote is found
			return total;

		}

		
		//sets the changed made the IaaS since it needs to update certain functions and needs to set the virtual machine listener and change the quote
		@Override
		public void setIaaSService(IaaSService myIaas) {
			thisIaas = myIaas;
			myIaas.subscribeToCapacityChanges(this);
			((IaaSForwarder) thisIaas).setQuoteProvider(this);
			((IaaSForwarder) thisIaas).setVMListener(this);
		}

		//Increases the number of machines by 15, it is a reactive function as it uses a sublist so it can react to any size iaas
		@Override
		public void increaseIaaSMachines(IaaSService iaas) {
			ArrayList<PhysicalMachine> halfOfTheMachines = new ArrayList<PhysicalMachine>(iaas.machines.subList(0, 15));	//Create an array list of first 15 machines 0 - 15
			for (PhysicalMachine pm : halfOfTheMachines) {		//Loop through the 15 machines and sets a physical machine for each
				
				//Simple debug print out for size
				System.out.println(iaas.machines.size());
				try {
					//Registers a new iaas
					iaas.registerHost(pm);
				} catch (Exception e) {
						System.out.println(e);	//Print out generic exception
				}
			}

		}

		
		//This function reduces the energy of the machine by a specified amount
		@Override
		public void reduceEnergy(IaaSService forService, double withHowManyWatts) throws SecurityException,
				InstantiationException, IllegalAccessException, NoSuchFieldException, IaaSHandlingException {
			double reduction = 0;
			do {
				ArrayList<PhysicalMachine> machineSetCopy = new ArrayList<PhysicalMachine>(forService.machines);	//New Physical Machine array list of all machines
				Collections.shuffle(machineSetCopy);	//Creates a copy of machines used for comparisons
				PhysicalMachine pmOriginal = machineSetCopy.get(0);	//Get the first machine as the original comparator
				PMForwarder pmReduced = createLowerEnergyMachine(pmOriginal);
				reduction += ((PMForwarder) pmOriginal).getMaxConsumption() - pmReduced.getMaxConsumption();	//We want to calculate how much we have reduced by
				try {
					//Replaces the old machine with the new less energy machine
					forService.deregisterHost(pmOriginal);	
					forService.registerHost(pmReduced);
				} catch (Exception e) {

				}
			} while (reduction < withHowManyWatts);	//Only stops running when we reach the target reduction

		}

		//Creates a new lower energy machine, by a non-specific amount
		@Override
		public PMForwarder createLowerEnergyMachine(PhysicalMachine energyReference)
				throws SecurityException, InstantiationException, IllegalAccessException, NoSuchFieldException {
			double referencePowerDraw = ((PMForwarder) energyReference).getMaxConsumption();	//Gets the energy of the machines
			PMForwarder pmNew;
			
			//This do while loop keeps making a new machine until it finds one using less energy, even though it could be by an insignificant amount
			do {
				pmNew = (PMForwarder) ExercisesBase.getNewPhysicalMachine();	
			} while (pmNew.getMaxConsumption() > referencePowerDraw);
			return pmNew;	//return our new less consumptive machine
		}

		
		//This method simply acts the same as the previous method except trying to find a cheaper machine
		@Override
		public PMForwarder createLowerPricedMachine(PMForwarder priceReference)
				throws SecurityException, InstantiationException, IllegalAccessException, NoSuchFieldException {
			PMPriceRecord recordForReference = new PMPriceRecord(priceReference);	
			PMPriceRecord recordForNew;		//Create an empty PMPriceRecord to use for comparisons
			do {
				recordForNew = new PMPriceRecord((PMForwarder) ExercisesBase.getNewPhysicalMachine());		//Gets a new machine for comparisons (hopefully cheaper)
			} while (recordForNew.getCurrentMachinePrice() > recordForReference.getCurrentMachinePrice());
			return recordForNew.pm;
		}

		
		//If a new machine is not added register one we need to keep our machines running and stop them being blank
		@Override
		public void capacityChanged(ResourceConstraints newCapacity, List<PhysicalMachine> affectedCapacity) {
			final boolean newMachineRegistered = thisIaas.isRegisteredHost(affectedCapacity.get(0));

			if (!newMachineRegistered) {		//If no machine is registered then we must loop through all the unregistered machines and re-register them with our variables
				for (int i = 0; i < affectedCapacity.size(); i++) {
					try {
						thisIaas.registerHost(ExercisesBase.getNewPhysicalMachine(RandomUtils.nextDouble(2, 5)));	//Creates a new machine between the double 2-5 which we believe gives us the most efficient results
					} catch (Exception e) {
						e.printStackTrace();	//Print errors
					}
				}
			}
		}

		
		//If our virtual machine has a state change, such as it is now destroyed we must be quick to get a new one online
		@Override
		public void stateChanged(VirtualMachine vm, State oldState, State newState) {
			
			//If our vm is now running make sure we set it up
			if (newState == VirtualMachine.State.RUNNING) {	
				vmHosts.put(vm, vm.getResourceAllocation().getHost());
			}

			
			//if vm stops running check if the host is empty, if its empty then leave it if its still registered on a destroyed machine we must make a new one
			if (newState == VirtualMachine.State.DESTROYED) {
				PhysicalMachine host = vmHosts.get(vm);

				// If the host is not found in the list, simply return.
				if (host == null) {
					return;
				}

				// If the host PM is still registered, the VM was surely
				// destroyed by the user.
				if (thisIaas.isRegisteredHost(host)) {
					vmsDestroyedByUser++;
				}
			}
		}

		
		//if a new vm is added we must alert our vm class of this statechange and tell it that we have one new machine
		@Override
		public void newVMadded(VirtualMachine[] vms) {
			vmsRequested += vms.length;

			for (VirtualMachine vm : vms) {
				vm.subscribeStateChange(this);
			}

		}

		
		//Reactive method to reduce machines by 15, taking a sublist of the first 15 elements and deregistering them
		public void reduceMachines(IaaSService iaas) {
			ArrayList<PhysicalMachine> halfOfTheMachines = new ArrayList<PhysicalMachine>(iaas.machines.subList(0, 15));
			try {
				for (PhysicalMachine pm : halfOfTheMachines) {
					iaas.deregisterHost(pm);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Unexpected things happened terminating...");
				System.exit(1);
			}			
		}
	}
